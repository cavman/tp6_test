<?php
declare (strict_types = 1);

namespace app\api\controller;
use Firebase\JWT\JWT as JWTUtil;
use think\facade\Log;
use think\facade\Queue;

class Index
{
    public function test()
    {
        Log::write('进入controller');
        Queue::later(5,'api/Testjob',$data='',$queue = null);
        return '您好！这是一个[api]示例应用';
    }

    //生成token
    public function createJwt($userId = 1)
    {
        $key = md5('zq8876!@!'); //jwt的签发密钥，验证token的时候需要用到
        $time = time(); //签发时间
        $expire = $time + 14400; //过期时间
        $token = array(
            "user_id" => $userId,
            "iss" => "http://www.najingquan.com/",//签发组织
            "aud" => "zhangqi", //签发作者
            "iat" => $time,
            "nbf" => $time,
            "exp" => $expire
        );
        $jwt = JWTUtil::encode($token, $key);
        // $decode = JWTUtil::decode($jwt, $key,['HS256']);
        // print_r($decode);
        return $jwt;
    }
}
