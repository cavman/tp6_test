<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2018 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------

use app\middleware\Jwt;
use think\facade\Route;

//当路由规则都不匹配的话，会路由到`miss`
// Route::miss(function() {
//     return show(10000);
// });
Route::get('test', 'index/test')->middleware(Jwt::class);
Route::get('createJwt/:userId', 'index/createJwt');
Route::group('auth', function(){
    
})->middleware(Jwt::class);
