<?php
namespace app\common\exception;

use think\exception\Handle;
use Throwable;
use think\Response;

class Http extends Handle
{
    public function render($request, Throwable $e):Response
    {
        // 请求异常
        if ($request->isAjax()) {
            return show(10001,$e->getMessage(),[],500);
        }else{
            exit($e->getMessage());
        }
    }

}